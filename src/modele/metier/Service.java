package modele.metier;

/**
 *
 * @author HugoLPB
 */
public class Service {
    private int code;           //code service
    private String designation; //designation du service
    private String email;       //e-mail du service
    private String telephone;   //numero de telephone du service
    
    public Service(int Code, String Designation, String Email, String Telephone) {
        this.code = Code;
        this.designation = Designation;
        this.email = Email;
        this.telephone = Telephone;
}

    @Override
    public String toString() {
        return "Service{" + "code=" + code + ", designation=" + designation +", email=" + email +", telephone=" + telephone + '}';
    }
    
    public int getCode() {
        return code;
    }
    
    public void setCode(int setCode) {
        this.code = code;
    }
    
    public String GetDesignation() {
        return designation;
    }
    
    public void setDesignation() {
        this.designation = designation;
    }
                    
    public String getEmail() {
        return email;
    }
    
    public void setEmail() {
        this.email = email;
    }
    
    public String getTelephone() {
        return telephone;
    }
    
    public void setTelephone() {
        this.telephone= telephone;
    }
}