package modele.metier;
import java.time.LocalDate;
import java.util.Date;
/**
 *
 * @author jjimenez
 */
public class Salarie {
     
    private String id;
    private String nom;                 //nom salarie
    private String prenom;              //prenom salarie
    private LocalDate dateNaiss;        //date naissance salarie
    private LocalDate dateEmbauche;     //date d'embauche salarie
    private String fonction;            //fonction salarie
    private double tauxHoraire ;        //taux horaire salarie
    private String situationFamiliale ; //situation familiale salarie
    private int nbrEnfants;             //nombre d'enfants du salarie
    private Categorie categorie;        //association classe categorie
    private Service service;            //association classe service

    public Salarie(String id, String nom, String prenom, LocalDate dateNaiss, LocalDate dateEmbauche, String fonction, double tauxHoraire, String situationFamiliale, int nbrEnfants, Categorie categorie, Service service) {
   
    this.id = id;
    this.nom = nom;             
    this.prenom = prenom;         
    this.dateNaiss = dateNaiss;
    this.dateEmbauche = dateEmbauche;
    this.fonction = fonction;
    this.tauxHoraire = tauxHoraire ;
    this.situationFamiliale = situationFamiliale;
    this.nbrEnfants = nbrEnfants;
    this.categorie = categorie;
    this.service = service;
    }

    @Override
    public String toString() {
        return "Salarie{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", dateNaiss=" + dateNaiss + ", dateEmbauche=" + dateEmbauche + ", fonction=" + fonction + ", tauxHoraire=" + tauxHoraire + ", situationFamiliale=" + situationFamiliale + ", nbrEnfants=" + nbrEnfants + ", categorie=" + categorie + ", service=" + service + '}';
    }

    public String getId() {
    return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getNom() {
    return nom;
    }

    public void setNom(String nom) {
    this.nom = nom;
    }

    public String getPrenom() {
    return prenom;
    }
    
    public void setPrenom(String prenom) {
    this.prenom = prenom;
    }

    public void setDateNaiss(LocalDate dateNaiss) {
    this.dateNaiss = dateNaiss;
    }
    
    public LocalDate getDateNaiss() {
    return dateNaiss;
    }
    
    public void setDateEmbauche(LocalDate dateEmbauche) {
    this.dateEmbauche = dateEmbauche;
    }
    
    public LocalDate getDateEmbauche() {
    return dateEmbauche;
    }
    
    public void setFonction(String fonction) {
    this.fonction = fonction;
    }
    
    public String getFonction() {
    return fonction;
    }
    
    public void setTauxHoraire(double tauxHoraire) {
    this.tauxHoraire = tauxHoraire;
    }
    
    public double getTauxHoraire() {
    return tauxHoraire;
    }
    
    public void setSituationFamiliale(String situationFamiliale) {
    this.situationFamiliale = situationFamiliale;
    }
    
    public String getSituationFamiliale() {
    return situationFamiliale;
    }
    
    public void setNbrEnfants(int nbrEnfants) {
    this.nbrEnfants = nbrEnfants;
    }
    
    public int getNbrEnfants() {
    return nbrEnfants;
    }
    
    public void setCategorie(Categorie categorie) {
    this.categorie = categorie;
    }
    
    public Categorie getCategorie() {
    return categorie;
    }
    
    public void setService(Service service) {
    this.service = service;
    }
    
    public Service getService() {
    return service;
    }

}
