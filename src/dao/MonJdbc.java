package dao;
import java.sql.*;

/**
 *
 * @author jjimenez
 */
public class MonJdbc {

    private static String url = "jdbc:mysql://localhost/sirh";
    private static String utilBD = "sirh_util";
    private static String mdpBD = "secret";
    private static Connection cnx; // java.sql.Connection

    /**
     * Retourner une connexion, la créer si elle n'existe pas...
     *
     * @return la connexion existante ou créée
     * @throws java.sql.SQLException
     */
    public static Connection connecter() throws SQLException {
        if (cnx == null) {
            cnx = DriverManager.getConnection(url, utilBD, mdpBD);
        }
        return cnx;
    }

    /**
     * Fermer la connexion et libérer les ressources
     *
     * @throws SQLException
     */
    public static void deconnecter() throws SQLException{
        if (cnx != null) {
            cnx.close();
        }
    }
    
    /**
     * test de connexion
     * @return true si la connexu=ion est établie (cnx != null)
     */
    public static boolean estConnecte(){
        return (cnx != null);
    }

    /**
     * Valorisation des attributs de connexion
     *
     * @param url
     * @param login
     * @param mdp
     */
    public static void initialiser(String url, String login, String mdp) {
        MonJdbc.url = url;
        utilBD = login;
        mdpBD = mdp;
    }

    /**
     * Valorisation des attributs de connexion dans le contexte de l'application
     *
     * @param typeSgbd = "mysql", "oracle" ou "derby"
     */
    public static void initialiser(String typeSgbd) {
        switch (typeSgbd) {
            case "oracle":
                initialiser("jdbc:oracle:thin:@localhost:1521:XE", "sirh_util", "secret");
                break;
            case "derby":
                initialiser("jdbc:derby://localhost:1527/sirh", "sirh_util", "secret");
                break;
            case "mysql":
            default:
                initialiser("jdbc:mysql://localhost:3306/sirh", "sirh_util", "secret");
                break;
        }

    }

    /**
     * Détermine le type de SGBD de la connexion courante
     * @return le nom du SGBD connecté
     * @throws SQLException
     */
    public static String versionSGBD() throws SQLException {
        String leSgbd = "Pas de connexion";
        if (cnx != null) {
            DatabaseMetaData dbmd = cnx.getMetaData();
            leSgbd = dbmd.getDatabaseProductName() /* + " " + dbmd.getDatabaseProductVersion()*/;
        }
        return leSgbd;
    }

}
