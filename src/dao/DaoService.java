package dao;
import modele.metier.Service;
import java.sql.*;
import java.util.*;
/**
 *
 * @author csourice
 */
public class DaoService {
    
    public static Service getOneById(int id) throws SQLException{
        Service unService = null;
        Connection cnx = MonJdbc.connecter();
        // préparer la requête
        String requete = "SELECT * FROM SERVICE WHERE CODE = ?";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        pstmt.setInt(1, id);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            int code = rs.getInt("CODE");
            String designation = rs.getString("DESIGNATION");
            String email = rs.getString("EMAIL");
            String telephone = rs.getString("TEL");
            unService = new Service(code, designation, email, telephone);
        }
        return unService;
    }
 

    public static List<Service> getAll() throws SQLException {
        List<Service> lesServices = new ArrayList<Service>();
        Service unService;
        Connection cnx = MonJdbc.connecter();
        // préparer la requête
        String requete = "SELECT * FROM SERVICE";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            int code = rs.getInt("CODE");
            String designation = rs.getString("DESIGNATION");
            String email = rs.getString("EMAIL");
            String telephone = rs.getString("TEL");
            unService = new Service(code, designation, email, telephone);
            lesServices.add(unService);
        }
        return lesServices;
    }
}
    