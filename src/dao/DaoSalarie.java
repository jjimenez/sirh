package dao;
import modele.metier.Salarie;
import modele.metier.Categorie;
import modele.metier.Service;
import java.sql.*;
import java.util.*;
import java.time.LocalDate;
/**
 *
 * @author csourice
 */
public class DaoSalarie {
    
    public static Salarie getOneById(String codeCat) throws SQLException{
         
        Salarie unSalarie = null;
        Connection cnx = MonJdbc.connecter();
        
        // préparer la requête
        String requete = "SELECT * FROM SALARIE WHERE CODE = ?";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        pstmt.setString(1, codeCat);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            
            String id = rs.getString("CODE");
            String nom = rs.getString("NOM");
            String prenom = rs.getString("PRENOM");
            String fonction = rs.getString("FONCTION");
            LocalDate dateNaiss = rs.getDate("dateNaiss").toLocalDate();
            LocalDate dateEmbauche = rs.getDate("DATEEMBAUCHE").toLocalDate();
            double tauxHoraire = rs.getDouble("TAUXHORAIRE");
            String situationFamiliale = rs.getString("SITUATIONFAMILIALE");
            int nbrEnfants = rs.getInt("NBRENFANTS");
            
            String numCat = rs.getString("NUMCAT");
            Categorie uneCategorie = DaoCategorie.getOneById(numCat);
            
            int numServ = rs.getInt("CODESERV");
            Service unService = DaoService.getOneById(numServ);


            
            unSalarie = new Salarie(id, nom, prenom,dateNaiss, dateEmbauche, fonction, tauxHoraire, situationFamiliale, nbrEnfants, uneCategorie, unService);  
        }
        return unSalarie;
    }
 
    /**
     * Extraction de toutes les adresses
     * @return collection de toutes les adresses trouvées dans la BD ; la collection peut être vide
     * @throws SQLException 
     */
    public static List<Salarie> getAll() throws SQLException {
        List<Salarie> lesSalaries = new ArrayList<Salarie>();
        Salarie unSalarie;
        Connection cnx = MonJdbc.connecter();
        // préparer la requête
        String requete = "SELECT * FROM SALARIE";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            
             String id = rs.getString("CODE");
            String nom = rs.getString("NOM");
            String prenom = rs.getString("PRENOM");
            String fonction = rs.getString("FONCTION");
            LocalDate dateNaiss = rs.getDate("dateNaiss").toLocalDate();
            LocalDate dateEmbauche = rs.getDate("DATEEMBAUCHE").toLocalDate();
            double tauxHoraire = rs.getDouble("TAUXHORAIRE");
            String situationFamiliale = rs.getString("SITUATIONFAMILIALE");
            int nbrEnfants = rs.getInt("NBRENFANTS");
            
            String numCat = rs.getString("NUMCAT");
            Categorie uneCategorie = DaoCategorie.getOneById(numCat);
            
            int numServ = rs.getInt("CODESERV");
            Service unService = DaoService.getOneById(numServ);
            
            unSalarie = new Salarie(id, nom, prenom,dateNaiss, dateEmbauche, fonction, tauxHoraire, situationFamiliale, nbrEnfants, uneCategorie, unService);  
            lesSalaries.add(unSalarie);
        }
        return lesSalaries;
    }
    
        public static List<Salarie> getAllByCodeService(int codeServ) throws SQLException {
            
        List<Salarie> lesSalaries = new ArrayList<Salarie>();
        Salarie unSalarie;
        Connection cnx = MonJdbc.connecter();
        // préparer la requête
        String requete = "SELECT * FROM SALARIE WHERE CODESERV = ?";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        pstmt.setInt(1, codeServ);
        ResultSet rs = pstmt.executeQuery();
        
        while (rs.next()) {
            
            String id = rs.getString("CODE");
            String nom = rs.getString("NOM");
            String prenom = rs.getString("PRENOM");
            String fonction = rs.getString("FONCTION");
            LocalDate dateNaiss = rs.getDate("dateNaiss").toLocalDate();
            LocalDate dateEmbauche = rs.getDate("DATEEMBAUCHE").toLocalDate();
            double tauxHoraire = rs.getDouble("TAUXHORAIRE");
            String situationFamiliale = rs.getString("SITUATIONFAMILIALE");
            int nbrEnfants = rs.getInt("NBRENFANTS");
            
            String numCat = rs.getString("NUMCAT");
            Categorie uneCategorie = DaoCategorie.getOneById(numCat);
            
            Service unService = DaoService.getOneById(codeServ);
            
            unSalarie = new Salarie(id, nom, prenom,dateNaiss, dateEmbauche, fonction, tauxHoraire, situationFamiliale, nbrEnfants, uneCategorie, unService);  
            lesSalaries.add(unSalarie);
        }
        return lesSalaries;
    }
}
