package dao;
import java.sql.*;
import java.util.*;
import modele.metier.Categorie;

/**
 *
 * @author jjimenez
 */
public class DaoCategorie {
    
     public static Categorie getOneById(String codeCat) throws SQLException{
         
        Categorie uneCategorie = null;
        Connection cnx = MonJdbc.connecter();
        
        // préparer la requête
        String requete = "SELECT * FROM CATEGORIE WHERE CODE = ?";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        pstmt.setString(1, codeCat);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            String code = rs.getString("CODE");
            String libelle = rs.getString("LIBELLE");
            double salaireMini = rs.getDouble("SALAIREMINI");
            String caisseRetraite = rs.getString("CAISSEDERETRAITE");
            int prime = rs.getInt("PRIMERESULTAT");
            
            uneCategorie = new Categorie(code, libelle, salaireMini, caisseRetraite, prime);
        }
        return uneCategorie;
    }

    public static List<Categorie> getAll() throws SQLException {
        List<Categorie> lesCategories = new ArrayList<Categorie>();
        Categorie uneCategorie;
        Connection cnx = MonJdbc.connecter();
        // préparer la requête
        String requete = "SELECT * FROM CATEGORIE";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            String code = rs.getString("CODE");
            String libelle = rs.getString("LIBELLE");
            double salaireMini = rs.getDouble("SALAIREMINI");
            String caisseRetraite = rs.getString("CAISSEDERETRAITE");
            int prime = rs.getInt("PRIMERESULTAT");
            
            uneCategorie = new Categorie(code, libelle, salaireMini, caisseRetraite, prime);
            lesCategories.add(uneCategorie);
        }
        return lesCategories;
    }
    
}
