package gui;

import dao.*;
import modele.metier.*;

import dao.MonJdbc;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;
import javax.swing.*;

public class JFrameSalarieParService extends javax.swing.JFrame {

    private DefaultComboBoxModel modeleComboServices;
    private DefaultComboBoxModel modeleComboSalaries;
    private Ecouteur leEcouteur;
    private RenduService leRenduService;
    private RenduSalarie leRenduSalarie;


    public JFrameSalarieParService() {
        initComponents();

        modeleComboServices = new DefaultComboBoxModel<Service>();
        jComboBoxServices.setModel(modeleComboServices);

        modeleComboSalaries = new DefaultComboBoxModel<Salarie>();
        jComboBoxSalaries.setModel(modeleComboSalaries);

        try {
            //- initialiser le modèle des données de la JComboBox
            remplirModeleComboService(modeleComboServices);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Constructeur JFrameClient - Pb SGBD \n" + ex.getMessage());
        }

        leRenduService = new RenduService();
        jComboBoxServices.setRenderer(leRenduService);       
        
        leRenduSalarie = new RenduSalarie();
        jComboBoxSalaries.setRenderer(leRenduSalarie);
        leEcouteur = new Ecouteur();
        jComboBoxServices.addActionListener(leEcouteur);
        jButtonConsulter.addActionListener(leEcouteur);
        
   }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jComboBoxServices = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jComboBoxSalaries = new javax.swing.JComboBox<>();
        jButtonConsulter = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Sélection du salarié"));

        jLabel1.setText("Choisissez un service");

        jLabel2.setText("Choisissez un salarié");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1)
                    .addComponent(jComboBoxSalaries, 0, 441, Short.MAX_VALUE)
                    .addComponent(jComboBoxServices, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jComboBoxServices, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addGap(13, 13, 13)
                .addComponent(jComboBoxSalaries, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(103, Short.MAX_VALUE))
        );

        jButtonConsulter.setText("Consulter");
        jButtonConsulter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonConsulterActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(199, 199, 199)
                        .addComponent(jButtonConsulter)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButtonConsulter)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonConsulterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonConsulterActionPerformed
        Salarie codeSalarie = (Salarie) modeleComboSalaries.getSelectedItem();
        String leCodeSalarie = codeSalarie.getId();

        JFrameInfosSalarie Jfc = new JFrameInfosSalarie(leCodeSalarie);
        Jfc.setVisible(true);
        
        this.setVisible(false);
        
    }//GEN-LAST:event_jButtonConsulterActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrameSalarieParService.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrameSalarieParService.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrameSalarieParService.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrameSalarieParService.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrameSalarieParService().setVisible(true);
            }
        });
    }

    private void remplirModeleComboService(DefaultComboBoxModel unModeleCombo) throws SQLException {
        List<Service> lesServices = DaoService.getAll();
        for (Service unService : lesServices) {
            unModeleCombo.addElement(unService);
        }
    }

    private void remplirModeleComboSalarie(DefaultComboBoxModel unModeleCombo, int codeServ) throws SQLException {
        List<Salarie> lesSalaries = DaoSalarie.getAllByCodeService(codeServ);
        for (Salarie unSalarie : lesSalaries) {
            unModeleCombo.addElement(unSalarie);
        }
    }

    private class Ecouteur implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
                      
            // Sélection du composant à l'origine de l'événement
            if (ae.getSource() == jComboBoxServices) {
                // sélection dans la liste déroulante
                Service unService = (Service) modeleComboServices.getSelectedItem();
                int leCodeServ = unService.getCode();     

                try {
                    modeleComboSalaries.removeAllElements();
                    remplirModeleComboSalarie(modeleComboSalaries, leCodeServ);
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Enregistrer - Pb SGBD \n" + ex.getMessage());
                }

            } 
                      


        }
    }

    /**
     * Gestionnaire de rendu : contrôler l'affichage du composant JComboBox
     */
    private class RenduService extends JLabel implements ListCellRenderer {

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            // définir le contenu textuel à afficher
            if (value != null) {
                Service unService = (Service) value;
                this.setText(unService.GetDesignation() + " ");
            } else {
                this.setText("");
            }
            if (isSelected) {
                setBackground(Color.black);
                setForeground(Color.white);

            } else {
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            }
            return this;
        }
    }

    private class RenduSalarie extends JLabel implements ListCellRenderer {

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            // définir le contenu textuel à afficher
            if (value != null) {
                Salarie unSalarie = (Salarie) value;
                this.setText(unSalarie.getNom() + " " + unSalarie.getPrenom());
            } else {
                this.setText("");
            }
            if (isSelected) {
                setBackground(Color.black);
                setForeground(Color.white);

            } else {
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            }
            return this;
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonConsulter;
    private javax.swing.JComboBox<String> jComboBoxSalaries;
    private javax.swing.JComboBox<String> jComboBoxServices;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

}
