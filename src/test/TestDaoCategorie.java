package test;

import java.sql.SQLException;
import java.util.List;
import dao.DaoCategorie;
import dao.MonJdbc;
import modele.metier.Categorie;

/**
 *
 * @author jjimenez
 */
public class TestDaoCategorie {

    public static void main(String[] args) {

        String sgbd = "mysql";
        java.sql.Connection cnx = null;

        try {
            System.out.println("Test0 : connexion");
            MonJdbc.initialiser(sgbd);
            cnx = MonJdbc.connecter();
            if (!MonJdbc.estConnecte()) {
                System.out.println("Echec de la connexion ");
            } else {
                System.out.println("connexion " + MonJdbc.versionSGBD() + " réussie : ");
                System.out.println("Test0 effectué\n");

                System.out.println("Test1 : sélection unique");
                Categorie cetteCategorie = DaoCategorie.getOneById("C1");
                System.out.println("Categorie d'identifiant : " + "C1" + " : " + cetteCategorie.toString());
                System.out.println("Test1 effectué\n");

                System.out.println("Test2 : sélection multiple");
                List<Categorie> desCategories = DaoCategorie.getAll();
                System.out.println("Les catégories lues : " + desCategories.toString());
                System.out.println("Test2 effectué\n");
            }

        } catch (SQLException e) {
            System.err.println("Erreur SQL : " + e.getMessage());
        } finally {
            try {
                MonJdbc.deconnecter();
            } catch (SQLException e) {
                System.err.println("Erreur de fermeture de la connexion JDBC : " + e);
            }
        }

    }

}
