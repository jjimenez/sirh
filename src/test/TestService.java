package test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import modele.metier.Service;

public class TestService {

public static void main(String[] args) {

//Test 1: Instanciation d'un objet servTest de type Service 
Service servTest = new Service(3, "Comptable", "compta@gmail.com", "0295811799");

//Test 2: Affichage toString de l'objet servTest de type Service
System.out.println(servTest);

//Test 3: Utilisation d'une méthode get sur cet objet
System.out.println("E-mail: " + servTest.getEmail());

//Test 4: Utilisation d'une méthode set sur cet objet
servTest.setCode(4);
System.out.println("Modification: " + servTest);
    }
}
