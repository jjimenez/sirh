package test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import modele.metier.Categorie;
import modele.metier.Salarie;
import modele.metier.Service;

public class TestSalarie {
    
public static void main(String[] args) {

//Test 1: Instanciation d'un objet salarieTest de type salarieTest 
DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
Salarie salarieTest = new Salarie("1", "Bon", "Jean", LocalDate.parse("1996-08-07", dtf), LocalDate.parse("2014-12-03", dtf), "Developpeur", 11.00, "Célibataire", 0, new Categorie ("1", "Informatique", 2000.00, "CNAV", 150), new Service(3, "Comptable", "compta@gmail.com", "0295811799"));

//Test 2: Affichage toString de l'objet salarieTest de type Salarie
System.out.println(salarieTest);

//Test 3: Utilisation d'une méthode get sur cet objet
System.out.println("Nom: " + salarieTest.getNom());

//Test 4: Utilisation d'une méthode set sur cet objet
salarieTest.setId("4");
System.out.println("Modification: " + salarieTest);

}    
}
