package test;

import java.sql.SQLException;
import java.util.List;
import dao.DaoService;
import dao.MonJdbc;
import modele.metier.Service;

/**
 *
 * @author jjimenez
 */
public class TestDaoService {

    public static void main(String[] args) {

        String sgbd = "mysql";
        java.sql.Connection cnx = null;

        try {
            System.out.println("Test0 : connexion");
            MonJdbc.initialiser(sgbd);
            cnx = MonJdbc.connecter();
            if (!MonJdbc.estConnecte()) {
                System.out.println("Echec de la connexion ");
            } else {
                System.out.println("connexion " + MonJdbc.versionSGBD() + " réussie : ");
                System.out.println("Test0 effectué\n");

                System.out.println("Test1 : sélection unique");
                Service ceService = DaoService.getOneById(1);
                System.out.println("Service d'identifiant : " + 1 + " : " + ceService.toString());
                System.out.println("Test1 effectué\n");

                System.out.println("Test2 : sélection multiple");
                List<Service> desServices = DaoService.getAll();
                System.out.println("Les services lus : " + desServices.toString());
                System.out.println("Test2 effectué\n");
            }

        } catch (SQLException e) {
            System.err.println("Erreur SQL : " + e.getMessage());
        } finally {
            try {
                MonJdbc.deconnecter();
            } catch (SQLException e) {
                System.err.println("Erreur de fermeture de la connexion JDBC : " + e);
            }
        }

    }

}
