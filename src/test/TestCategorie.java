package test;

import modele.metier.Categorie;

public class TestCategorie {
    
public static void main(String[] args) {

//Test 1: Instanciation d'un objet catTest de type Categorie    
Categorie catTest = new Categorie("1", "Informatique", 2000.00, "CNAV", 150);

//Test 2: Affichage toString de l'objet catTest de type Categorie
System.out.println(catTest);

//Test 3: Utilisation d'une méthode get sur cet objet
System.out.println("Code: " + catTest.getCode());

//Test 4: Utilisation d'une méthode set sur cet objet
catTest.setCode("4");
System.out.println("Modification: " + catTest);

}    
}
