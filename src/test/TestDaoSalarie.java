package test;

import java.sql.SQLException;
import java.util.List;
import dao.DaoSalarie;
import dao.MonJdbc;
import modele.metier.Salarie;

/**
 *
 * @author jjimenez
 */
public class TestDaoSalarie {

    public static void main(String[] args) {

        String sgbd = "mysql";
        java.sql.Connection cnx = null;

        try {
            System.out.println("Test0 : connexion");
            MonJdbc.initialiser(sgbd);
            cnx = MonJdbc.connecter();
            if (!MonJdbc.estConnecte()) {
                System.out.println("Echec de la connexion ");
            } else {
                System.out.println("connexion " + MonJdbc.versionSGBD() + " réussie : ");
                System.out.println("Test0 effectué\n");

                System.out.println("Test1 : sélection unique");
                Salarie ceSalarie = DaoSalarie.getOneById("S01");
                System.out.println("Salarié d'identifiant : " + "S01" + " : " + ceSalarie.toString());
                System.out.println("Test1 effectué\n");

                System.out.println("Test2 : sélection multiple");
                List<Salarie> desSalaries = DaoSalarie.getAll();
                System.out.println("Les salariés lus : " + desSalaries.toString());
                System.out.println("Test2 effectué\n");
                
                System.out.println("Test3 : sélection avec code service");
                List<Salarie> salariesServ = DaoSalarie.getAllByCodeService(3);
                System.out.println("Les salariés lus : " + salariesServ.toString());
                System.out.println("Test3 effectué\n");
                
            }

        } catch (SQLException e) {
            System.err.println("Erreur SQL : " + e.getMessage());
        } finally {
            try {
                MonJdbc.deconnecter();
            } catch (SQLException e) {
                System.err.println("Erreur de fermeture de la connexion JDBC : " + e);
            }
        }

    }

}
